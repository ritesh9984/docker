package com.mtech.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class WelcomeController {

	@GetMapping("/")
    public String welcomePage(Model model) {
		model.addAttribute("title", "It's working...");
		return "welcome";
    }
     
    @PostMapping("/adduser")
    public String addUser(BindingResult result, Model model) {
    	
        if (result.hasErrors()) {
            return "add-user";
        }
        
        model.addAttribute("users", null);
        return "index";
    }
}
